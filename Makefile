requirements:
	@echo 'Installing requirements...'
	pip install -r requirements.txt

syncdb:
	@echo 'Syncronizing db...'
	python manage.py syncdb

migrate:
	@echo 'Migrate db...'
	python manage.py migrate

run: requirements syncdb migrate
	python manage.py runserver
