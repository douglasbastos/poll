#coding: utf-8

from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic

class HomeView(generic.TemplateView):
    template_name = 'home/base_site.html'

    def get_context_data(self, **kwargs):
        dados = super(HomeView, self).get_context_data(**kwargs)
        dados['descricao'] = 'Selecione uma das opções do menu acima.'

        return dados