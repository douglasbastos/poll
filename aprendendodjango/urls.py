from django.conf.urls import patterns, include, url

from django.contrib import admin
from aprendendodjango.views import HomeView
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', HomeView.as_view(), name='home'),
    # url(r'^$', 'include(admin.site.urls))
    # url(r'^blog/', include('blog.urls')),
    # url(r'^', include('minhaapp.urls', namespace="minhaapp")),
    url(r'^minhaapp/', include('minhaapp.urls', namespace="minhaapp")),
    url(r'^galeria/', include('galeria.urls', namespace="galeria")),
    url(r'^admin/', include(admin.site.urls)),
)
