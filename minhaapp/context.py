from django.conf import settings

from minhaapp.models import Choice, Poll
from galeria.models import Galeria

def context_processors(request):
    ret = {}

    ret['settings'] = settings
    ret['latest_poll_list'] = Poll.objects.order_by('-pub_date')[:10]
    ret['latest_galeria_list'] = Galeria.objects.all()

    return ret